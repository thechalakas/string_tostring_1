﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace String_ToString_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Water_Bottle_1 bottle1 = new Water_Bottle_1(10,20);
            Water_Bottle_2 bottle2 = new Water_Bottle_2(10, 20);

            //this will give me a proper string sentence
            Console.WriteLine(bottle1.ToString());
            //this will simply display the type of the object
            Console.WriteLine(bottle2.ToString());

            //this is to avoid console from vanishing
            Console.ReadLine();
        }
    }


    //this does not have its own implementation of tostring
    class Water_Bottle_2
    {
        private int quantity;
        private int height;

        public Water_Bottle_2(int q, int h)
        {
            quantity = q;
            height = h;
        }
    }
    
    //this class has its own implementation of tostring
    class Water_Bottle_1
    {
        private int quantity;
        private int height;

        public Water_Bottle_1(int q,int h)
        {
            quantity = q;
            height = h;
        }

        //I am using the override keyword
        //this allows me to give my class its own implementation of tostring
        public override string ToString()
        {
            string text_to_return = " quantity is " + quantity + "height is " + height;
            return text_to_return;
        }
    }
}
